# Nith's KISS Scripts
# by IcyNith
# Licensed under The Unlicense
#

# ncheat
ncheat() {
	if [ -z "$1" ]; then
		echo "Usage: ncheat <command>"
	else
		curl https://cheat.sh/"$@"
	fi
}

# nextract
nextract() {
	if [ -z "$1" ]; then
  		echo "Usage: nextract <path/file_name>.<7z|bz2|gz|lzma|rar|tar|tar.bz2|tar.gz|tar.xz|tbz2|tgz|xz|Z|zip>"
	else
    		if [ -f "$1" ]; then
        	case $1 in
          		*.7z)        7z x ../$1        ;;
          		*.bz2)       bunzip2 ../$1     ;;
          		*.gz)        gunzip ../$1      ;;
          		*.lzma)      unlzma ../$1      ;;
          		*.rar)       unrar x -ad ../$1 ;;
          		*.tar)       tar xvf ../$1     ;;
          		*.tar.bz2)   tar xvjf ../$1    ;;
          		*.tar.gz)    tar xvzf ../$1    ;;
          		*.tar.xz)    tar xvJf ../$1    ;;
          		*.tbz2)      tar xvjf ../$1    ;;
          		*.tgz)       tar xvzf ../$1    ;;
          		*.xz)        unxz ../$1        ;;
          		*.Z)         uncompress ../$1  ;;
          		*.zip)       unzip ../$1       ;;
          		*)           echo "extract: '$1' - unknown archive method" ;;
        	esac
    		else
        		echo "$1 - file does not exist"
    		fi
	fi
}

# nmaketar
nmaketar() {
	if [ -z "$1" ]; then
 		echo "Usage: nmaketar <folder/file_name>"
	else
  		tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"
	fi
}

# nmakezip
nmakezip() {
	if [ -z "$1" ]; then
  		echo "Usage: nmakezip <folder/file_name>"
	else
  		zip -r "${1%%/}.zip" "$1"
	fi
}

# ntransfer
ntransfer() {
	if [ -z "$1" ]; then
		echo "Usage: ntransfer <file_name>"
	else
		tmpfile=$( mktemp -t transferXXX )
		curl --progress-bar --upload-file $1 https://transfer.sh/$(basename $1) >> $tmpfile
		cat $tmpfile
		echo " "
		rm -f $tmpfile
	fi
}

# nweather
nweather() {
	if [ -z "$1" ]; then
  		echo "Usage: nweather <city>"
  		echo "Without a city, your IP's city will be taken."
  		curl https://wttr.in/
	else
  		curl https://wttr.in/"${@}"
	fi
}
