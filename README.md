# Nith's KISS Scripts

Just a collection of scripts I like to use.

## Scripts

<details>
<summary>ncheat</summary>
Fast way to look up what a command does

`ncheat <command>`
</details>

<details>
<summary>nextract</summary>
Extract the most common archive files easily

`nextract <path/file_name>.<7z|bz2|gz|lzma|rar|tar|tar.bz2|tar.gz|tar.xz|tbz2|tgz|xz|Z|zip>`
</details>

<details>
<summary>nmaketar</summary>
Makes a .tar file quick and easy

`nmaketar <folder/file_name>`
</details>

<details>
<summary>nmakezip</summary>
Makes a .zip file quick and easy

`nmakezip <folder/file_name>`
</details>


<details>
<summary>ntransfer</summary>
Uploads given file to transfer.sh

`ntransfer <file_name>`
</details>

<details>
<summary>nweather</summary>
Get the weather report into your terminal

`nweather` or `nweather <city>`
</details>

## Used API's
<details>
<summary>API's</summary>

* nweather: Weather based on a location: [wttr](https://wttr.in)
* ncheat: Cheatsheets for commands and languages: [cheat](https://cheat.sh/)
* ntransfer: Upload provided: [transfer.sh](https://transfer.sh)
</details>

## Install
Take a script you like (or all) and copy them into your `$PATH`

## nkisss as oh-my-zsh plugin
- copy the `oh-my-zsh-plugin/nkisss` folder into `$ZSH/custom/plugins/`
- edit your `.zshrc`
- look for `plugins=(<some_entry>)`
- put in `plugins=(<some_entry> nkisss)`
